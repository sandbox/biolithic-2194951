<?php
/**
 * Configuration Form for Admin to set the secret and publishable keys.
 *
 * @return array
 *   Array of form values to be saved.
 */
function paywallbraintree_admin_settings()
{
    $form = array(
        '#type' => 'fieldset',
        '#title' => t('My Module Settings'),
        '#collapsible' => TRUE,
        '#collapsed' => FALSE
    );
    
    $form['paywallbraintree_readme'] = array(
        '#type' => 'markup',
        '#title' => t('Read me instructions'),
        '#markup' => '<p>' . l(t('Read me instructions'), 'admin/config/services/paywallbraintree/readme') . '</p>'
    );
    
    $form['paywallbraintree_warning'] = array(
        '#type' => 'markup',
        '#title' => t('What happens when my customer --buys now--?'),
        '#markup' => '<p> This module does not save ANYTHING about the credit cards or payment to this website.  The user may type it into the form on this website which gets sent immediately only to Braintree encrypted and you will only get a pass/fail message back.  Do not save anything about the payment for re-occuring billing, record keeping, or anything like that to this site.  You may view any details about each transaction at your Braintree account page. <br>&nbsp;<br>This module requires Javascript to be enabled for your site through out the process to work properly.  Your customers cannot buy products through your website using this module if they have disabled Javascript in their browser. </p>'
    );
    
    $form['paywallbraintree_whatits'] = array(
        '#type' => 'markup',
        '#title' => t('What is this module?'),
        '#markup' => '<p> PaywallStripe produces a content type with a built-in checkout/credit card processor to sell single-file digital content like e-books.  Drupal Commerce is recommended, but if that suite is too large for your needs, this is a simple payment solution.</p>'
    );
    
    $form['paywallbraintree_wherefrom'] = array(
        '#type' => 'markup',
        '#title' => t('Where to get the TEST information for this page from?'),
        '#markup' => '<p>' . l(t('Sign up for a Braintree Payments test account:'), 'https://www.braintreepayments.com/get-started') . '</p>'
    );
    
    $form['paywallbraintree_wherefrom2'] = array(
        '#type' => 'markup',
        '#title' => t('Where to get the LIVE information for this page from?'),
        '#markup' => '<p>' . l(t('Sign up for a Braintree Payments live account:'), 'https://braintreepayments.com') . '</p>'
    );
    
    $form['paywallbraintree_environment'] = array(
        '#type' => 'textfield',
        '#title' => t('Braintree environment setting'),
        '#default_value' => variable_get('paywallbraintree_environment', ""),
        '#description' => t('Braintree environment setting'),
        '#required' => TRUE
    );
    
    $form['paywallbraintree_merchantId_testkey'] = array(
        '#type' => 'textfield',
        '#title' => t('Braintree Test Merchant Id'),
        '#default_value' => variable_get('paywallbraintree_merchantId_testkey', ""),
        '#description' => t('Braintree Test Merchant Id'),
        '#required' => TRUE
    );
    
    $form['paywallbraintree_merchantId_livekey'] = array(
        '#type' => 'textfield',
        '#title' => t('Braintree Live Merchant Id'),
        '#default_value' => variable_get('paywallbraintree_merchantId_livekey', ""),
        '#description' => t('Braintree Live Merchant Id'),
        '#required' => TRUE
    );
    
    $form['paywallbraintree_publishable_testkey'] = array(
        '#type' => 'textfield',
        '#title' => t('Braintree test public API Key'),
        '#default_value' => variable_get('paywallbraintree_publishable_testkey', ""),
        '#description' => t('Braintree test public API Key'),
        '#required' => TRUE
    );
    
    $form['paywallbraintree_publishable_livekey'] = array(
        '#type' => 'textfield',
        '#title' => t('Braintree live public API Key'),
        '#default_value' => variable_get('paywallbraintree_publishable_livekey', ""),
        '#description' => t('Braintree live public API Key'),
        '#required' => TRUE
    );
    
    $form['paywallbraintree_secret_testkey'] = array(
        '#type' => 'textfield',
        '#title' => t('Braintree test secret API Key'),
        '#default_value' => variable_get('paywallbraintree_secret_testkey', ""),
        '#description' => t('Braintree test secret API Key'),
        '#required' => TRUE
    );
    
    $form['paywallbraintree_secret_livekey'] = array(
        '#type' => 'textfield',
        '#title' => t('Braintree live secret API Key'),
        '#default_value' => variable_get('paywallbraintree_secret_livekey', ""),
        '#description' => t('Braintree live secret API Key'),
        '#required' => TRUE
    );
    
    $form['paywallbraintree_price'] = array(
        '#type' => 'textfield',
        '#title' => t('Price in USA dollars of download'),
        '#default_value' => variable_get('paywallbraintree_price', ""),
        '#description' => t('Price in USA dollars of download'),
        '#required' => TRUE
    );
    
    $form['paywallbraintree_gonelive'] = array(
        '#type' => 'checkbox',
        '#title' => t('Live?'),
        '#default_value' => variable_get('paywallbraintree_gonelive', ""),
        '#description' => t('Check this box to use your live key.  Uncheck to enter demo mode and use your test key')
    );
    
    $form['paywallbraintree_sslmessage'] = array(
        '#type' => 'checkbox',
        '#title' => t('SSL error message?'),
        '#default_value' => variable_get('paywallbraintree_sslmessage', ""),
        '#description' => t('This is not recommended, but you can get away with using this module on non-SSL/http servers.  To show an error message at the top of this page while you dont have an HTTPS secure connection, so that site admins know, check this box.')
    );
    
    $form['paywallbraintree_sendemail'] = array(
        '#type' => 'checkbox',
        '#title' => t('Send emails to admin and customer for each successful transaction?'),
        '#default_value' => variable_get('paywallbraintree_sendemail', ""),
        '#description' => t('Check this box to send emails.  Uncheck to not send emails.')
    );
    
    foreach (array_keys(node_type_get_names()) as $term) {
        $form["paywallbraintree_$term"] = array(
            '#type' => 'checkbox',
            '#title' => t("Do you want to charge for $term ?"),
            '#default_value' => variable_get("paywallbraintree_$term", ""),
            '#description' => t("Uncheck to allow all users to view nodes of content type $term .  Check to limit access to paying members.")
        );
    }
    
    return system_settings_form($form);
}